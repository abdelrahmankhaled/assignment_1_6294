package servicesPackage;

import activityPackage.*;

import java.util.*;
import java.util.stream.Collectors;
import java.util.Map.Entry;

// IN REAL WORLD APPS THIS SHOULD POST TO API OR SAVE TO DB

public class ActivityService {
    public static ArrayList<Activity> Activities = new ArrayList<Activity>();
    private double currentHeartRate = 80;
    private int burntCalories = 0;
    private double currentHeartInc = 0;

    public int getBurntCalories() {
        return burntCalories;
    }

    public double getCurrentHeartInc() {
        return currentHeartInc;
    }

    public Activity createNewActivity(String activityName, String timeText)
    {
        Activity activity;
        int time = Integer.parseInt(timeText);
        switch (activityName){
            case "Running": activity = new Running(time); break;
            case "Swimming": activity = new Swimming(time); break;
            case "Kickboxing": activity = new Kickboxing(time); break;
            case "Strength Training": activity = new StrengthTraining(time); break;
            default: throw new Error("Not enough data is provided");
        }

        burntCalories += activity.calculateBurntCalories();
        currentHeartInc += activity.calculateIncreaseInBeats(currentHeartRate);
        currentHeartRate += activity.calculateIncreaseInBeats(currentHeartRate);
        activity.setHeartRate(currentHeartRate);
        return activity;
    }

    public int calculateTotalBurntCalories()
    {
        int burntCalories = 0;
        for (Activity activity: ActivityService.Activities)
        {
            burntCalories += activity.calculateBurntCalories();
        }
        return burntCalories;
    }

    public HashMap<String, double[]> totalForEachActivity()
    {
        HashMap<String, double[]> activityHashMap = new HashMap<String, double[]>();
        double[] burnHeart;
        for (Activity activity: ActivityService.Activities)
        {
            String key = activity.getClass().getSimpleName();
            if (activityHashMap.containsKey(key)) {
                burnHeart = activityHashMap.get(key);
                burnHeart[0] += activity.calculateBurntCalories();
                double n = activity.calculateIncreaseInBeats(currentHeartRate);
                currentHeartRate += n;
                activity.setHeartRate(currentHeartRate);
                burnHeart[1] += n;
            } else {
                burnHeart = new double[2];
                burnHeart[0] = activity.calculateBurntCalories();
                burnHeart[1] = activity.calculateIncreaseInBeats(currentHeartRate);
                currentHeartRate += burnHeart[1];
                activity.setHeartRate(currentHeartRate);
            }
            activityHashMap.put(key, burnHeart);
        }

        Utils utils = new Utils();

        HashMap<String, double[]> sortedMap =
                activityHashMap.entrySet().stream()
                        .sorted(utils.valueComparator)
                        .collect(Collectors.toMap(Entry::getKey, Map.Entry::getValue,
                                (e1, e2) -> e1, LinkedHashMap::new));

        return sortedMap;
    }

}
