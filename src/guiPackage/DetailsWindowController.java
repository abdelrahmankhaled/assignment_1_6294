package guiPackage;

import activityPackage.Activity;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import servicesPackage.ActivityService;


import java.net.URL;
import java.util.ResourceBundle;

public class DetailsWindowController implements Initializable {

    public ListView<String> activitiesListView;

    public ListProperty<String> listProperty = new SimpleListProperty<>();
    public Label totalCaloriesBurnt;
    public Label totalHeartRateInc;
    public Label currentHeartBeats;

    private ActivityService _activityService;

    public DetailsWindowController() {
        _activityService = new ActivityService();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        populateListView();
        setTotalRates();
    }

    private void populateListView()
    {
        ObservableList<String> items = FXCollections.observableArrayList();
        for (Activity activity:ActivityService.Activities)
        {
            items.add(activity.toString());
        }

        activitiesListView.itemsProperty().bind(listProperty);
        listProperty.set(items);
    }

    private void setTotalRates()
    {
        totalCaloriesBurnt.setText(String.valueOf(_activityService.calculateTotalBurntCalories()));

        int lastElementIndex = ActivityService.Activities.size() - 1;
        double lastHeartRate = ActivityService.Activities.get(lastElementIndex).getHeartRate();

        totalHeartRateInc.setText(String.valueOf(lastHeartRate - 80));
        currentHeartBeats.setText(String.valueOf(lastHeartRate));
    }
}
