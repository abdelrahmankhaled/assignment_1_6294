package guiPackage;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import servicesPackage.ActivityService;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

public class rankingWindowController implements Initializable {
    public ListView activitiesRankingListView;

    private ActivityService _activityService;

    public ListProperty<String> listProperty = new SimpleListProperty<>();


    public rankingWindowController()
    {
        _activityService = new ActivityService();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        populateListView();

    }

    private void populateListView() {
        ObservableList<String> items = FXCollections.observableArrayList();
        HashMap<String, double[]> map =  _activityService.totalForEachActivity();
        int ranking = 1;

        for (String key: map.keySet())
        {
            String text = "Ranking: " + ranking++ + ", " + key + ", Burnt Calories: " + map.get(key)[0] + ", Heart Rate: " + map.get(key)[1];
            items.add(text);
        }

        activitiesRankingListView.itemsProperty().bind(listProperty);
        listProperty.set(items);
    }
}
